import './Modal.css'

const Modal = props => (
    <div className='backdrop'>
        <div className='modal'>{props.children}</div>
    </div>
)

export default Modal