import './TasksList.css'

const TasksList = ({tasks, onDone, onEdit, onRemove}) => {
    const sortedArr = tasks.sort((a, b) => {
        const firstDate = a.date
        const secondDate = b.date

        return Date.parse(firstDate) - Date.parse(secondDate)
    })
    
    const list = sortedArr.map((task, index) => {
        const mainStyle = `task ${onDone ? null : 'done-task'}`
        const dayStyle = `day ${task.day}`

        return <div key={index} className={mainStyle}>
            {onDone ? <p onClick={() => onDone(task.id)} className='done'>DONE </p> : null}
            <p className='time'>{task.startHour}</p>
            <p>{task.name}</p>
            <p className='details'>{task.details}</p>
            <p className={dayStyle}>{task.day}</p>
            {onEdit ? <p onClick={() => onEdit(task)} className='edit'>I</p> : null}
            {onRemove ? <p onClick={() => onRemove(task.id)} className='delete'>x</p> : null}
        </div>
    })

    return list
}

export default TasksList
