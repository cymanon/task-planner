import { useState, useContext } from 'react'
import moment from 'moment'

import './TaskForm.css'
import Context from '../store/context'

const TaskForm = ({ onCancel, taskId = null, taskDay = '', taskName = '', taskDetails = '', taskDate = '', taskStartHour = '', type} ) => {
    const context = useContext(Context)

    const [name, setName] = useState(taskName)
    const [details, setDetails] = useState(taskDetails)
    const [startHour, setStartHour] = useState(taskStartHour || new Date().toLocaleTimeString().slice(0, 5))
    const [date, setDate] = useState(taskDate)
    const [day, setDay] = useState(taskDay)

    const onSubmitHandler = event => {
        event.preventDefault()
        
        const id = taskId ?? `TASK_${Math.floor(Math.random() * 1000)}`

        const newTask = {
            id,
            day,
            name,
            details,
            startHour,
            date
        }

        if (type === 'add') {
            context.addTask(newTask)
        } else if (type === 'edit') {
            context.editTask(newTask)
        }

        onCancel()
    }

    const dateHandler = value => {
        setDate(value)
        setDay(moment(value).format('ddd'))
    }

    return (
        <form onSubmit={onSubmitHandler}>
            <label>Task: 
                <input 
                    type='text'
                    value={name}
                    onChange={(event) => setName(event.target.value)}/></label>
            <label>Details: 
                <input 
                    type='text'
                    value={details}
                    onChange={(event) => setDetails(event.target.value)}/></label>
            <div className='time'>
                <label>Starting time:
                    <input
                        type='time'
                        value={startHour}
                        placeholder='min'
                        onChange={(event) => setStartHour(event.target.value)} /></label>
                <label>Date: 
                    <input
                        className='date'
                        type='date'
                        value={date}
                        onChange={(event) => dateHandler(event.target.value)} /></label>
            </div>
            <button className='add' type='submit' disabled={ !day|| !name || !startHour || !date }>Add</button>
            <button className='cancel' onClick={onCancel}>Cancel</button>
        </form>
    )
}

export default TaskForm