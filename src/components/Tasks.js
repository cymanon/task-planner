import { useState, useContext } from 'react'

import Modal from './Modal'
import TaskForm from './TaskForm'
import TasksList from './TasksList'
import Context from '../store/context'

const Tasks = () => {
    const context = useContext(Context)
    const [editedTask, setEditedTask] = useState(null)

    const editTaskHandler = task => setEditedTask(task)

    const editModal = editedTask ?
        <Modal><TaskForm
            type='edit'
            taskDay={editedTask.day}
            taskName={editedTask.name}
            taskDetails={editedTask.details}
            taskDate={editedTask.date}
            taskStartHour={editedTask.startHour}
            taskId={editedTask.id}
            onCancel={() => setEditedTask(null)}/></Modal> :
        null

    const tasks = context.tasks.length ?
        <TasksList 
            tasks={context.tasks}
            onDone={context.taskDone}
            onEdit={editTaskHandler}
            onRemove={context.removeTask}/>:
        <p>Add new task</p>

    const doneTasks = context.done.length ?
    <TasksList tasks={context.done} /> :
    null
    
    return (
        <>
            {tasks}
            {doneTasks}
            {editModal}
        </>
    )
}

export default Tasks
