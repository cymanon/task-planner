import { useReducer } from 'react'
import Context from './context'

const defaultState = {
    tasks: [],
    done: []
}

const reducer = (state, action) => {
    if (action.type === 'ADD') {
        const newTasks = [...state.tasks].concat(action.item)
        
        return {
            ...state,
            tasks: newTasks
        }
    }
    if (action.type === 'REMOVE') {
        const newTasks = [...state.tasks].filter(task => task.id !== action.id)
        return {
            ...state,
            tasks: newTasks
        }
    }
    if (action.type === 'EDIT') {
        const id = state.tasks.findIndex(task => task.id === action.item.id)
        const newTasks = [...state.tasks]
        newTasks[id] = action.item
        return {
            ...state,
            tasks: newTasks
        }
    }
    if (action.type === 'DONE') {
        const newTasks = [...state.tasks]
        const done = newTasks.find(task => task.id === action.id)
        const doneTask = [...state.done].concat(done)
        const filteredTasks = newTasks.filter(task => task.id !== action.id)

        return {
            ...state,
            tasks: filteredTasks,
            done: doneTask
        }
    }
    return defaultState
}

const Provider = props => {
    const [stateSnapshot, dispatchAction] = useReducer(reducer, defaultState)

    const addTaskHandler = item => {
        dispatchAction({type: 'ADD', item})
    }

    const removeTaskHandler = id => {
        dispatchAction({type: 'REMOVE', id})
    }

    const editTaskHandler = item => {
        dispatchAction({type: 'EDIT', item})
    }

    const doneTaskHandler = id => {
        dispatchAction({ type: 'DONE', id})
    }
    
    const context = {
        tasks: stateSnapshot.tasks,
        done: stateSnapshot.done,
        addTask: addTaskHandler,
        removeTask: removeTaskHandler,
        editTask: editTaskHandler,
        taskDone: doneTaskHandler,
    }

    return <Context.Provider value={context}>
        {props.children}
    </Context.Provider>
}

export default Provider