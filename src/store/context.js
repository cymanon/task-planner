import React from 'react'

const Context = React.createContext({
    tasks: [],
    done: [],
    addTask: (task) => {},
    removeTask: (id) => {},
    taskDone: (id) => {},
    editTask: (task) => {}
})

export default Context