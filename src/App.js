import { useState } from 'react'
import './App.css'

import ContextProvider from './store/provider'
import Tasks from './components/Tasks'
import Modal from './components/Modal'
import TaskForm from './components/TaskForm'

function App() {
  const [showAddPanel, setShowAddPanel] = useState(false)

  const showAddPanelHandler = () => setShowAddPanel(prevState => !prevState)
  
  return (
    <ContextProvider>
      <div className='app'>
        <button onClick={showAddPanelHandler} className='add-button'>+</button>
        <Tasks />
        {showAddPanel ? <Modal><TaskForm type='add' onCancel={showAddPanelHandler}/></Modal> : null}
      </div>
    </ContextProvider>
  )
}

export default App